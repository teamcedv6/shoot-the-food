#include "OptionsState.h"

void OptionsState::enter () {
	auto viewPort = _gameManager->getViewport();
	viewPort->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

void OptionsState::exit () {

}

void OptionsState::pause() {

}

void OptionsState::resume() {
	auto viewPort = _gameManager->getViewport();
	viewPort->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool OptionsState::frameStarted (const Ogre::FrameEvent& evt) {

	return true;

}

bool OptionsState::frameEnded (const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}
