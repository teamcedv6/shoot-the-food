#include "PauseState.h"

void PauseState::enter () {
	auto viewPort = _gameManager->getViewport();
	viewPort->setBackgroundColour(Ogre::ColourValue(0.0, 1.0, 0.0));
}

void PauseState::exit () {

}

void PauseState::pause () {

}

void PauseState::resume () {

}

bool PauseState::frameStarted (const Ogre::FrameEvent& evt) {

	return true;

}

bool PauseState::frameEnded (const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}
