#include "PlayState.h"
#include "MainMenuState.h"

void PlayState::setLose(bool lose) {
	_lose = lose;
}

void PlayState::enter () {
	_nodes = _gameManager->getNodeManager();
	_actions = _gameManager->getActionsManager();
	_move = _gameManager->getMovementSystem();
	_cmSystem = _gameManager->getCameraSystem();
	_phSystem = _gameManager->getPhysicsSystem();
	_aSystem = _gameManager->getAnimationSystem();
	_grSystem = _gameManager->getGraphicsSystem();
	_ui = _gameManager->getUserInterfaceSystem();

	_fov = _gameManager->getEntity("pov");

	_ui->loadUI("GameUI", "GameUI.xaml");

	_gameLogic = _gameManager->getGameLogic();

	auto ui = _ui->getUI("GameUI");

	_ui->deactivateCursor();

	Noesis::TextBlock * fasterText = (Noesis::TextBlock *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("fasterText");
	fasterText->SetOpacity(0);
	Noesis::Storyboard * fasterAnim = (Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("faster");
	_gameLogic->setFasterAnim(fasterAnim);

	ChangeBullets::setGameLogic(_gameLogic);
	ChangeBullets::setBulletText((Noesis::TextBlock *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("bulletText"));
	ChangeBullets::setBulletUp((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("bulletUp"));
	ChangeBullets::setBulletDown((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("BulletDown"));

	ChangeLifes::setGameLogic(_gameLogic);
	ChangeLifes::addLifeUpAnimation((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("lifeUp1"));
	ChangeLifes::addLifeUpAnimation((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("lifeUp2"));
	ChangeLifes::addLifeUpAnimation((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("lifeUp3"));
	ChangeLifes::addLifeUpAnimation((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("lifeUp4"));
	ChangeLifes::addLifeUpAnimation((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("lifeUp5"));
	ChangeLifes::addLifeDownAnimation((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("lifeDown1"));
	ChangeLifes::addLifeDownAnimation((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("lifeDown2"));
	ChangeLifes::addLifeDownAnimation((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("lifeDown3"));
	ChangeLifes::addLifeDownAnimation((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("lifeDown4"));
	ChangeLifes::addLifeDownAnimation((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("lifeDown5"));

	ChangeScore::setGameLogic(_gameLogic);
	ChangeScore::setScoreText((Noesis::TextBlock *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("scoreText"));

	ChangeSpeed::setGameLogic(_gameLogic);
	ChangeSpeed::setFasterText(fasterText);
	ChangeSpeed::setFasterAnim(fasterAnim);

	ChangeElapsedTime::setTimeText((Noesis::TextBlock *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("timeText"));

	ChangeRoundTime::setFewTimeAnim((Noesis::Storyboard *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("fewTime"));
	ChangeRoundTime::setRoundText((Noesis::TextBlock *)((Noesis::UserControl*)ui->root.GetPtr())->FindName("roundTimeText"));

	auto initUI = std::make_shared<Action>();
	initUI->addSubaction(std::make_shared<ChangeBullets>(15, false));
	initUI->addSubaction(std::make_shared<ChangeLifes>(5, false));
	initUI->addSubaction(std::make_shared<ChangeScore>(0, false));
	
	_actions->addAction(initUI);


	
	auto soundManager = _gameManager->getSoundManager();
	soundManager->selectResource(SoundManager::MUSIC,"music",SoundManager::PLAY,50);

}

void PlayState::exit () {
	ChangeBullets::setGameLogic(nullptr);
	ChangeBullets::setBulletText(nullptr);
	ChangeBullets::setBulletUp(nullptr);
	ChangeBullets::setBulletDown(nullptr);

	ChangeLifes::setGameLogic(nullptr);
	ChangeLifes::clearAnimations();

	ChangeScore::setGameLogic(nullptr);
	ChangeScore::setScoreText(nullptr);

	ChangeSpeed::setGameLogic(nullptr);
	ChangeSpeed::setFasterText(nullptr);
	ChangeSpeed::setFasterAnim(nullptr);

	_ui->unloadUI("GameUI");
}

void PlayState::pause() {

}

void PlayState::resume() {
	auto viewPort = _gameManager->getViewport();
	viewPort->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool PlayState::frameStarted (const Ogre::FrameEvent& evt) {
	auto deltaTime = evt.timeSinceLastFrame;
	
	_gameLogic->update(_gameManager->getConfig(),_nodes,_grSystem,_phSystem.get(),_actions,_gameManager->_entities,deltaTime);

	_move->update(deltaTime);
	_phSystem->update(deltaTime);
	_aSystem->update(deltaTime);
	_ui->update(deltaTime);

	_actions->update(deltaTime);
	
	if(_lose) {
		_lose = false;
		_gameManager->getSoundManager()->selectResource(SoundManager::MUSIC,"menu",SoundManager::PLAY,50);
		_gameManager->getSoundManager()->selectResource(SoundManager::FX,"lose",SoundManager::PLAY,50);

		static_cast<MainMenuState*>(_gameManager->getState(GameState::States::MAIN_MENU).get())->setRedirectScore(true);
		popState(_gameManager);
	}
	
	return true;
}

bool PlayState::frameEnded (const Ogre::FrameEvent& evt) {
	auto deltaTime = evt.timeSinceLastFrame;
	_phSystem->update(deltaTime);
	
	return true;
}

void PlayState::movePoV(const OIS::MouseEvent & e) {
	float rotx = e.state.X.rel * -1;
	float roty = e.state.Y.rel * -1;

	shared_ptr<Action> action = std::make_shared<Action>();
	shared_ptr<MovePoV> pov = std::make_shared<MovePoV>( _fov,_nodes, rotx, roty, 0);
	action->addSubaction(pov);

	_actions->addAction(action);
}

void PlayState::shoot(const OIS::MouseEvent & e, InputBindings::PushState state) {

	if(_gameLogic->getBullets() <= 0 && state == InputBindings::RELEASE)
		_gameManager->getSoundManager()->selectResource(SoundManager::FX,"empty_shoot",SoundManager::PLAY,50);

	if(_gameLogic->getBullets() > 0 && state == InputBindings::RELEASE) {

		// Play sound	
		auto soundManager = _gameManager->getSoundManager();
		soundManager->selectResource(SoundManager::FX,"shoot_gun",SoundManager::PLAY,50);
		
		// Create bullet
		Obj::Options_t options;
		options.addNode = true;
		options.attachNode = true;
		options.addGraphic = true;
		options.addPhysic = true;
		options.typeAct = Obj::NEW;
		options.typeObj = Obj::CUSTOM;

		std::make_shared<Obj>(_gameManager->getConfig(),_gameManager->getNodeManager(),_gameManager->getGraphicsSystem(),
			_gameManager->getPhysicsSystem().get(),_gameManager->getActionsManager(),_gameManager->_entities,options);


		// Change bullet number
		auto changeBullets = std::make_shared<ChangeBullets>(-1, true);
		auto action = std::make_shared<Action>();
		action->addSubaction(changeBullets);

		_actions->addAction(action);
	}

}

void PlayState::exitGame(InputBindings::PushState pushState) {

	if(pushState == InputBindings::PushState::RELEASE){
		_exitGame = true;
		_ui->terminateUI();
	}

}

