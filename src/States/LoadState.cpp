#include "LoadState.h"

void LoadState::enter () {
	auto viewPort = _gameManager->getViewport();
	viewPort->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

void LoadState::exit () {

}

void LoadState::pause() {

}

void LoadState::resume() {
	auto viewPort = _gameManager->getViewport();
	viewPort->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool LoadState::frameStarted (const Ogre::FrameEvent& evt) {

	return true;

}

bool LoadState::frameEnded (const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}
