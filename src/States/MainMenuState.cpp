#include "MainMenuState.h"

void MainMenuState::enter () {

	auto sM = _gameManager->getSceneManager();

	// Cargamos la escena
	auto dotSceneLoader = std::make_shared<Ogre::DotSceneLoader>(_gameManager,_gameManager->getConfig());
	dotSceneLoader->parseDotScene("MainCharacter.scene","Shoot-the-food",sM);

	// Game Logic
	auto gL = std::make_shared<GameLogic>(_gameManager->getConfig(),_gameManager->getSoundManager(),sM,_gameManager->getNodeManager());
	_gameManager->setGameLogic(gL);

	// Asignamos música
	auto soundManager = _gameManager->getSoundManager();
	soundManager->selectResource(SoundManager::MUSIC,"menu",SoundManager::PLAY,50);

	// Shadow, Background, Ambientlight
	sM->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	sM->getCamera("pov")->getViewport()->setBackgroundColour(Ogre::ColourValue(0,0.5,1));
	sM->setAmbientLight(Ogre::ColourValue(0.75,0.75,0.75));
	
	// Creación del sol mediante particulas
	Ogre::ParticleSystem* sunParticle = _gameManager->getSceneManager()->createParticleSystem("Sun", "Space/Sun");
	Ogre::SceneNode* particleNode = _gameManager->getSceneManager()->getRootSceneNode()->createChildSceneNode("Particle");
	particleNode->setPosition(160,80,25);
	particleNode->attachObject(sunParticle);

	// Cargamos la interfaz
	_ui = _gameManager->getUserInterfaceSystem();
	_loadGui();
}

void MainMenuState::exit() {
	_ui->terminateUI();
}

void MainMenuState::pause () {
	_ui->unloadUI("MainMenu");
}

void MainMenuState::resume () {

	if(_redirectScore){
		_redirectScore = false;
		static_cast<ScoreState*>(_gameManager->getState(GameState::States::SCORE).get())->setType(ScoreState::WRITE);
		pushState(_gameManager, _gameManager->getState(GameState::States::SCORE));
	} else {
		_loadGui();
	}

}

void MainMenuState::_loadGui() {

	_ui->loadUI("MainMenu", "MainMenu.xaml");
	
	// Load  menu events handlers for each button (actions focus, unfocus and click
	auto uiRoot = _ui->getUI("MainMenu")->root.GetPtr();
	auto b1 = ((Noesis::UserControl*)uiRoot)->FindName("newBtn");
	((Noesis::Button*)b1)->MouseEnter() += Noesis::MakeDelegate(this, &MainMenuState::menuFocus);
	((Noesis::Button*)b1)->MouseLeave() += Noesis::MakeDelegate(this, &MainMenuState::menuUnfocus);
	((Noesis::Button*)b1)->MouseDown() += Noesis::MakeDelegate(this, &MainMenuState::menuClick);
	auto b2 = ((Noesis::UserControl*)uiRoot)->FindName("scoreBtn");
	((Noesis::Button*)b2)->MouseEnter() += Noesis::MakeDelegate(this, &MainMenuState::menuFocus);
	((Noesis::Button*)b2)->MouseLeave() += Noesis::MakeDelegate(this, &MainMenuState::menuUnfocus);
	((Noesis::Button*)b2)->MouseDown() += Noesis::MakeDelegate(this, &MainMenuState::menuClick);
	auto b3 = ((Noesis::UserControl*)uiRoot)->FindName("authorBtn");
	((Noesis::Button*)b3)->MouseEnter() += Noesis::MakeDelegate(this, &MainMenuState::menuFocus);
	((Noesis::Button*)b3)->MouseLeave() += Noesis::MakeDelegate(this, &MainMenuState::menuUnfocus);
	((Noesis::Button*)b3)->MouseDown() += Noesis::MakeDelegate(this, &MainMenuState::menuClick);
	auto b4 = ((Noesis::UserControl*)uiRoot)->FindName("exitBtn");
	((Noesis::Button*)b4)->MouseEnter() += Noesis::MakeDelegate(this, &MainMenuState::menuFocus);
	((Noesis::Button*)b4)->MouseLeave() += Noesis::MakeDelegate(this, &MainMenuState::menuUnfocus);
	((Noesis::Button*)b4)->MouseDown() += Noesis::MakeDelegate(this, &MainMenuState::menuClick);

	_ui->activateCursor();
}

bool MainMenuState::frameStarted (const Ogre::FrameEvent& evt)  {

	auto deltaTime = evt.timeSinceLastFrame;
	_ui->update(deltaTime);

	return true;
}

bool MainMenuState::frameEnded (const Ogre::FrameEvent& evt) {
	if (_exitGame) {
		_ui->terminateUI();
		return false;
	}


	return true;
}

void MainMenuState::exitGame(InputBindings::PushState pushState) {

	if(pushState == InputBindings::PushState::RELEASE){
		_exitGame = true;
	}

}

void MainMenuState::menuFocus(Noesis::BaseComponent * sender, const Noesis::MouseEventArgs & args) {
	std::stringstream ss;
	const std::string & btnName = ((Noesis::UserControl*)sender)->GetName();
	auto ui = _ui->getUI("MainMenu");

	std::cout << "FOCUS " << btnName.substr(0, btnName.length()-3) << "\n";

	ss << btnName.substr(0, btnName.length()-3) << "_focus";

	auto ani = ((Noesis::UserControl*)ui->root.GetPtr())->FindName(ss.str().c_str());
	assert(ani != nullptr);
	((Noesis::Storyboard *) ani)->Begin();

	// Focus Sound
	auto soundManager = _gameManager->getSoundManager();
	soundManager->selectResource(SoundManager::FX,"button_hover",SoundManager::PLAY,50);
}

void MainMenuState::menuUnfocus(Noesis::BaseComponent * sender, const Noesis::MouseEventArgs & args) {
	std::stringstream ss;
	const std::string & btnName = ((Noesis::UserControl*)sender)->GetName();
	auto ui = _ui->getUI("MainMenu");

	std::cout << "UNFOCUS " << btnName.substr(0, btnName.length()-3) << "\n";

	ss << btnName.substr(0, btnName.length()-3) << "_unfocus";

	auto ani = ((Noesis::UserControl*)ui->root.GetPtr())->FindName(ss.str().c_str());
	assert(ani != nullptr);
	((Noesis::Storyboard *) ani)->Begin();
}

void MainMenuState::menuClick(Noesis::BaseComponent * sender, const Noesis::MouseButtonEventArgs & args) {
	//if(args.)
	std::stringstream ss;
	const std::string & btnName = ((Noesis::UserControl*)sender)->GetName();
	auto ui = _ui->getUI("MainMenu");

	const std::string & shortName = btnName.substr(0, btnName.length()-3);
	std::cout << shortName << "\n";

	ss << btnName.substr(0, btnName.length()-3) << "_push";

	auto ani = ((Noesis::UserControl*)ui->root.GetPtr())->FindName(ss.str().c_str());
	((Noesis::Storyboard *) ani)->Begin();

	// Click sound
	auto soundManager = _gameManager->getSoundManager();
	soundManager->selectResource(SoundManager::FX,"shoot_gun",SoundManager::PLAY,50);

	if(shortName == "new") {
		pushState(_gameManager, _gameManager->getState(GameState::States::PLAY));
	}
	else if(shortName == "score") {
		static_cast<ScoreState*>(_gameManager->getState(GameState::States::SCORE).get())->setType(ScoreState::READ);
		pushState(_gameManager, _gameManager->getState(GameState::States::SCORE));

	}
	else if(shortName == "author") {
		pushState(_gameManager, _gameManager->getState(GameState::States::AUTHOR));
	}
	else if(shortName == "exit") {
		popState(_gameManager);
	}
	else {

	}

}

void MainMenuState::setRedirectScore(bool redirect) {
	_redirectScore = redirect;
}
