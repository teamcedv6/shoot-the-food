#include "IngameMenuState.h"

void IngameMenuState::enter () {
	auto viewPort = _gameManager->getViewport();
	viewPort->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

void IngameMenuState::exit () {

}

void IngameMenuState::pause() {

}

void IngameMenuState::resume() {
	auto viewPort = _gameManager->getViewport();
	viewPort->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 1.0));
}

bool IngameMenuState::frameStarted (const Ogre::FrameEvent& evt) {

	return true;

}

bool IngameMenuState::frameEnded (const Ogre::FrameEvent& evt) {
	if (_exitGame)
		return false;

	return true;
}
