#include <EntityComponentSystem/Components/MovableComponent.h>

MovableComponent::MovableComponent() {}

MovableComponent::MovableComponent(const MovableComponent & Movable) : _movableParts(Movable._movableParts) {}

MovableComponent::MovableComponent(const MovableComponent && Movable) : _movableParts(Movable._movableParts){}

MovableComponent::~MovableComponent(){}

MovableComponent & MovableComponent::operator= (const MovableComponent & Movable){
	_movableParts = Movable._movableParts;
	return  *this;
}

void MovableComponent::addMovablePart(const std::string & name, Ogre::SceneNode * node,bool isPhysicalManaged) {
	MovementParameters params;
	params._node = node;
	params._physicalMovement = isPhysicalManaged;
	_movableParts[name] = params;
}

void MovableComponent::deleteMovablePart(const std::string & name) {
	//_MovableParts.erase(std::find(_MovableParts.begin(), _MovableParts.end(), name));
}
