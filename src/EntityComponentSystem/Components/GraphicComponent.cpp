#include "GraphicComponent.h"

std::shared_ptr<Ogre::SceneManager> GraphicComponent::_sceneManager;

GraphicComponent::GraphicComponent() { assert(_sceneManager != nullptr); }

GraphicComponent::GraphicComponent(const GraphicComponent & graphics) : _entities(graphics._entities) {}

GraphicComponent::GraphicComponent(const GraphicComponent && graphics) : _entities(graphics._entities){}

GraphicComponent::~GraphicComponent(){}

GraphicComponent & GraphicComponent::operator= (const GraphicComponent & graphics){
	_entities = graphics._entities;
	return  *this;
}

void GraphicComponent::addGraphic(const std::string & name, const std::string & mesh, Ogre::SceneNode * node, bool castShadows) {
	Ogre::Entity * entity = _sceneManager->createEntity(name, mesh);

	if(name == "Plane") {
		entity->setMaterialName("Ground");
	}

	node->attachObject(entity);
	entity->setCastShadows(castShadows);
	_entities[name] = entity;
}

void GraphicComponent::addStaticGraphic(const std::string & name, const std::string & mesh, bool castShadows) {
	Ogre::StaticGeometry * staticGeom = _sceneManager->createStaticGeometry(name);
	Ogre::Entity * staticEnt = _sceneManager->createEntity(name, mesh);

	staticEnt->setCastShadows(castShadows);

	staticGeom->addEntity(staticEnt, Ogre::Vector3(0,0,0));
	staticGeom->build();  // Operacion para construir la geometria

}

void GraphicComponent::removeGraphic(const std::string & name) {
	Ogre::Entity * entity = _entities[name];
	//std::remove()
	entity->getParentSceneNode()->detachObject(entity);
	_sceneManager->destroyEntity(entity);
}

void GraphicComponent::setSceneManager(std::shared_ptr<Ogre::SceneManager> sceneManager) {
	_sceneManager = sceneManager;
}
