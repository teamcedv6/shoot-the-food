#include "UserInterfaceComponent.h"

UserInterfaceComponent::UserInterfaceComponent() : _xaml("IsSoEmptyWithoutMe") {}

UserInterfaceComponent::UserInterfaceComponent(const std::string & xaml) : _xaml(xaml) {}

UserInterfaceComponent::UserInterfaceComponent(const UserInterfaceComponent & userInterface) : _xaml(userInterface._xaml) {}

UserInterfaceComponent::UserInterfaceComponent(const UserInterfaceComponent && userInterface) : _xaml(userInterface._xaml) {}

UserInterfaceComponent::~UserInterfaceComponent() {}

UserInterfaceComponent & UserInterfaceComponent::operator= (const UserInterfaceComponent & userInterface) { return *this;}
