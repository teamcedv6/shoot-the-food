#include "Entity.h"

std::shared_ptr<IdNameMapper> Entity::_mapper;

Entity::Entity() : Entity("default") {}

Entity::Entity(const std::string & name) : _name(name), _id((assert(_mapper!= nullptr), _mapper->createId(name))){}

Entity::Entity(const Entity & entity) : _name(entity._name), _id(entity._id) {}

Entity::Entity(Entity && entity) : _name(entity._name), _id(entity._id){}

Entity::~Entity(){
	_mapper->removeId(_id);
}

Entity & Entity::operator=(const Entity & entity){ return *this;}

const Entity::ID Entity::getId(){ return _id; }

const std::string & Entity::getName() { return _name; }

void Entity::setMapper(std::shared_ptr<IdNameMapper> mapper) { _mapper = mapper; }


