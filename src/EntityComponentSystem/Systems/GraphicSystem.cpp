#include "GraphicSystem.h"

GraphicSystem::GraphicSystem() : System(){}

GraphicSystem::GraphicSystem(std::shared_ptr<Ogre::SceneManager> sceneManager) :
		System(), _sceneManager(sceneManager) {}

GraphicSystem::GraphicSystem(const GraphicSystem & graphicSystem) : System(graphicSystem){
	_sceneManager = graphicSystem._sceneManager;
}

GraphicSystem::GraphicSystem(const GraphicSystem && graphicSystem) : System(graphicSystem) {
	_sceneManager = graphicSystem._sceneManager;
}

GraphicSystem::~GraphicSystem() {}

GraphicSystem & GraphicSystem::operator= (const GraphicSystem & graphicSystem) { std::cout << "Copy asign\n"; return *this; }
GraphicSystem & GraphicSystem::operator= (const GraphicSystem && graphicSystem) { std::cout << "Move asign\n"; return *this; }
