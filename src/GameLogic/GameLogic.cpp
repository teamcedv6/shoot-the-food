	#include "GameLogic.h"

GameLogic::GameLogic(){
}

GameLogic::GameLogic(std::shared_ptr<Configuration> configuration,std::shared_ptr<SoundManager> soundManager,
		std::shared_ptr<Ogre::SceneManager> sceneManager, std::shared_ptr<NodeManager> nodeManager){

	_round = 1;
	_totalTime = 0;
	_score = 0;
	_lifes = 5;
	_bulletNumber = 15;

	_elapsedCountDown = 0;
	_elapsedTotalTime = 0;

	_configuration = configuration;
	_soundManager = soundManager;

	// Recorremos los la configuración de cañones del config
	std::map<std::string,Configuration::Canon_t> configCannons = configuration->getCanonList();
	for(auto& kv : configCannons) {
		// Establecemos los contadores
		_times[kv.first] = kv.second.time;				// Tiempo acumulativo para comprobar delay de lanzamiento
		_splitTimes[kv.first] = kv.second.splitTime;	// Delay de tiempo entre disparos
		_speedUps[kv.first] = kv.second.speedUp;		// Resta del delay del tiempo cada aumento de dificultad
		_activeCannons[kv.first] = kv.second.active;	// Cañones activos
		
		// Creación de particulas para cada cañon
		std::ostringstream oss;
		oss << "particle_" << kv.first ;
		std::string nameParticleSystem = oss.str();

		_particleSystem[nameParticleSystem] = sceneManager->createParticleSystem(nameParticleSystem,"ringOfFire");
		//_particleSystem[nameParticleSystem]->setCastShadows(true);
		//_particleSystem[nameParticleSystem]->setKeepParticlesInLocalSpace(true);
		_particleSystem[nameParticleSystem]->getEmitter(0)->setEnabled(false);
		_particleSystem[nameParticleSystem]->getEmitter(0)->setDirection(kv.second.directonLauch);

		oss.clear();
		oss << "particleNode_" << kv.first ;
		std::string nameParticleNode = oss.str();

		auto psNode = nodeManager->createNode(nameParticleNode);
		psNode->setPosition(nodeManager->getNode(kv.first)->getPosition() + kv.second.launchPosition);
		//psNode->setScale(0.1,0.1,0.1);
		psNode->attachObject(_particleSystem[nameParticleSystem]);
		nodeManager->attachNode(psNode,"root");
		
	}

	// Cambio de velocidad
	_countDown = configuration->getCountDown();
	_countDownInit = _countDown;
}


GameLogic::GameLogic(const GameLogic & gameLogic) {
	_times = gameLogic._times;
	_countDown = gameLogic._countDown;
	_totalTime = gameLogic._totalTime;
	_round = gameLogic._round;
	_countDownInit = gameLogic._countDownInit;
	_score = gameLogic._score;
	_lifes = gameLogic._lifes;
	_bulletNumber = gameLogic._bulletNumber;
}

GameLogic::GameLogic(const GameLogic && gameLogic) {
	_times = gameLogic._times;
	_countDown = gameLogic._countDown;
	_totalTime = gameLogic._totalTime;
	_round = gameLogic._round;
	_countDownInit = gameLogic._countDownInit;
	_score = gameLogic._score;
	_lifes = gameLogic._lifes;
	_bulletNumber = gameLogic._bulletNumber;
}

GameLogic::~GameLogic (){}

GameLogic& GameLogic::operator = (const GameLogic &gameLogic){
	_times = gameLogic._times;
	_countDown = gameLogic._countDown;
	_totalTime = gameLogic._totalTime;
	_round = gameLogic._round;
	_countDownInit = gameLogic._countDownInit;
	_score = gameLogic._score;
	_lifes = gameLogic._lifes;
	_bulletNumber = gameLogic._bulletNumber;

	return *this;
}

void GameLogic::resetGame() {

	_round = 1;
	_totalTime = 0;
	_score = 0;
	_lifes = 5;
	_bulletNumber = 15;
	_elapsedCountDown = 0;
	_elapsedCountDown = 0;

	// Recorremos los la configuración de cañones del config
	std::map<std::string,Configuration::Canon_t> configCannons = _configuration->getCanonList();
	for(auto& kv : configCannons) {
		// Establecemos los contadores
		_times[kv.first] = kv.second.time;				// Tiempo acumulativo para comprobar delay de lanzamiento
		_splitTimes[kv.first] = kv.second.splitTime;	// Delay de tiempo entre disparos
		_speedUps[kv.first] = kv.second.speedUp;		// Resta del delay del tiempo cada aumento de dificultad
		_activeCannons[kv.first] = kv.second.active;	// Cañones activos
	}

	// Cambio de velocidad
	_countDown = _configuration->getCountDown();
	_countDownInit = _countDown;
}

void GameLogic::update(std::shared_ptr<Configuration> config, std::shared_ptr<NodeManager> nodes,
				std::shared_ptr<GraphicSystem> graphic, PhysicSystem* physic,
				std::shared_ptr<ActionsManager> actions, std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> entities,
				Ogre::Real deltaTime) {


		_elapsedTotalTime += deltaTime;
		_totalTime += deltaTime; // Tiempo total

		_elapsedCountDown += deltaTime;
		_countDown -= deltaTime; // Tiempo para siguiente aumento de velocidad, nueva ronda
		if(_countDown <= 0) {
			_round += 1;
		}

		if(_elapsedTotalTime > 1) {
			// Actualizar reloj
			_elapsedTotalTime = 0;

			std::shared_ptr<Action> updateTime = std::make_shared<Action>();
			std::shared_ptr<ChangeElapsedTime> sub = std::make_shared<ChangeElapsedTime>((int)_totalTime);
			updateTime->addSubaction(sub);

			actions->addAction(updateTime);
		}

		if(_elapsedCountDown > 1) {
			//Actualizar count down

			_elapsedCountDown = 0;

			std::shared_ptr<Action> updateTime = std::make_shared<Action>();
			std::shared_ptr<ChangeRoundTime> sub = std::make_shared<ChangeRoundTime>((int)_countDown);
			updateTime->addSubaction(sub);

			actions->addAction(updateTime);
		}

		// Cada ronda activamos un cañon, hasta tener los cuatro activos
		if(_round==2) {
			_activeCannons["canon_FoodCanon2"] = true;
		}else if(_round==3) {
			_activeCannons["canon_FoodCanon3"] = true;
		}else if(_round >=4) {
			_activeCannons["canon_FoodCanon4"] = true;
		}
		
		// Aumentamos el tiempo solamente en los cañones activos
		for(auto& kv : _times) {
			if(_activeCannons[kv.first])
				_times[kv.first] += deltaTime;
		}

		// Preparamos objeto a lanzar
		Obj::Options_t options;
		options.addNode = true;
		options.attachNode = true;
		options.addGraphic = true;
		options.addPhysic = true;
		options.addImpulse = true;
		options.typeAct = Obj::NEW;
		options.typeObj = Obj::FOOD;

		for (auto& kv : _configuration->getCanonList()) {
			// Si el cañon está activo
			if(_activeCannons[kv.first] && (_times[kv.first] >= _splitTimes[kv.first])){
				// Activamos particulas, sonido y reseteamos tiempo
				_times[kv.first] = 0;
				options.launcher = nodes->getNode(kv.first)->getPosition() + kv.second.launchPosition;
				options.vector = kv.second.directonLauch;
				// Objeto
				std::make_shared<Obj>(config,nodes,graphic,physic,actions,entities,options);
				// Sonido
				_soundManager->selectResource(SoundManager::FX,"shoot_cannon",SoundManager::PLAY,50);
				// Particulas
				std::ostringstream oss;
				oss << "particle_" << kv.first ;
				std::string nameParticleSystem = oss.str();
				_particleSystem[nameParticleSystem]->getEmitter(0)->setEnabled(true);
			}
		}


		if(_countDown <= 0) {
			// Nueva ronda
			for (auto& kv : _configuration->getCanonList()) {
				// A los cañones activos les restamos el delay entre disparos sin llegar a 0
				if(_activeCannons[kv.first] && (_splitTimes[kv.first] - _speedUps[kv.first] >= 0)){
					_splitTimes[kv.first] -= _speedUps[kv.first];
				}
			}

			// Animacion faster activa
			_fasterAnim->Begin();

			// Resetear nueva ronda
			_countDown = _countDownInit;
			// Crear objetos
			options.typeObj = Obj::POWERUP;
			Configuration::Canon_t cfgOptions = _configuration->getCanonList()["canon_PowerupCanon"];
			options.launcher = nodes->getNode("canon_PowerupCanon")->getPosition() + cfgOptions.launchPosition - Ogre::Vector3(1,0,0);
			options.vector = cfgOptions.directonLauch;
			// Reproducir sonidos
			_soundManager->selectResource(SoundManager::FX,"shoot_cannon",SoundManager::PLAY,50);
			_soundManager->selectResource(SoundManager::FX,"speed_up",SoundManager::PLAY,50);
			// Activar particulas
			_particleSystem["particle_canon_PowerupCanon"]->getEmitter(0)->setEnabled(true);
			std::make_shared<Obj>(config,nodes,graphic,physic,actions,entities,options);
			options.launcher = nodes->getNode("canon_PowerupCanon")->getPosition() + cfgOptions.launchPosition + Ogre::Vector3(1,0,0);
			std::make_shared<Obj>(config,nodes,graphic,physic,actions,entities,options);
			options.launcher = nodes->getNode("canon_PowerupCanon")->getPosition() + cfgOptions.launchPosition + Ogre::Vector3(2,0,0);
			std::make_shared<Obj>(config,nodes,graphic,physic,actions,entities,options);
		}
}

int GameLogic::getLifes() {return _lifes;}

int GameLogic::getScore() {return _score;}

int GameLogic::getBullets() {return _bulletNumber; }

int GameLogic::getRounds(){return _round;}

Ogre::Real GameLogic::getTotalTiem(){return _totalTime;}

void GameLogic::setScore(int score) {_score = score;}

void GameLogic::setLifes(int lifes) {_lifes = lifes;}

void GameLogic::setBullets(int bullets) {_bulletNumber = bullets;}

void GameLogic::setFasterAnim(Noesis::Storyboard * fasterAnim) { _fasterAnim = fasterAnim; }

void GameLogic::addDelayTime(float time) {
	for (auto& kv : _configuration->getCanonList()) {
		// A los cañones activos les aumentamos el delay entre disparos
		if(_activeCannons[kv.first]) {
			_splitTimes[kv.first] += time;
		}
	}
}

int GameLogic::_getIntRandom(int init, int end) {
	int random = 0;
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dis(init, end);
	random = dis(gen);
	return random;
}

float GameLogic::_getRealRandom(float init, float end) {
	float random = 0.0;
	std::random_device rd;	
	std::mt19937 gen(rd());
	std::uniform_real_distribution<float> dis(init, end);
	random = dis(gen);
	return random;
}
