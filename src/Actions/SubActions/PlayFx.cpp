#include "PlayFx.h"

PlayFx::PlayFx(){}

PlayFx::PlayFx(std::shared_ptr<SoundManager> soundManager, std::string fxKey) :
		_soundManager(soundManager), _fxKey(fxKey) {}

PlayFx::PlayFx(const PlayFx & playFx) {
	_soundManager = playFx._soundManager;
}

PlayFx::PlayFx(const PlayFx && playFx) {
	_soundManager = playFx._soundManager;
}

PlayFx::~PlayFx() {}

PlayFx & PlayFx::operator= (const PlayFx & playFx) {
	_soundManager = playFx._soundManager;

	return *this;
}


Action::ActionResult PlayFx::update(Ogre::Real deltaTime) {

	_soundManager->selectResource(SoundManager::FX,_fxKey,SoundManager::PLAY,50);

	_hasFinished = true;

	return Action::ACTION_FINISHED;
}
