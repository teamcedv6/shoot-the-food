#include "ChangeBullets.h"

Noesis::TextBlock * ChangeBullets::_bulletText = nullptr;
Noesis::Storyboard * ChangeBullets::_bulletUp = nullptr;
Noesis::Storyboard * ChangeBullets::_bulletDown = nullptr;
std::shared_ptr<GameLogic> ChangeBullets::_gameLogic = nullptr;

ChangeBullets::ChangeBullets() : ChangeBullets(0, true) {

}

ChangeBullets::ChangeBullets(int amount, bool relative) : _amount(amount), _relative(relative) {
	assert(_bulletText != nullptr);
	assert(_bulletUp != nullptr);
	assert(_bulletDown != nullptr);
	assert(_gameLogic != nullptr);
}

ChangeBullets::ChangeBullets(const ChangeBullets & changeBullets) {
	_amount = changeBullets._amount;
	_relative = changeBullets._relative;
}

ChangeBullets::ChangeBullets(const ChangeBullets && changeBullets) {
	_amount = changeBullets._amount;
	_relative = changeBullets._relative;
}

ChangeBullets::~ChangeBullets() {}

ChangeBullets & ChangeBullets::operator= (const ChangeBullets & changeBullets) {
	_amount = changeBullets._amount;
	_relative = changeBullets._relative;

	return *this;
}

void ChangeBullets::setBulletText(Noesis::TextBlock * bulletText) {	_bulletText = bulletText;}

void ChangeBullets::setBulletUp(Noesis::Storyboard * bulletUp) { _bulletUp = bulletUp; }

void ChangeBullets::setBulletDown(Noesis::Storyboard * bulletDown) { _bulletDown = bulletDown; }

void ChangeBullets::setGameLogic(std::shared_ptr<GameLogic> gameLogic) { _gameLogic = gameLogic; }

Action::ActionResult ChangeBullets::update(Ogre::Real deltaTime) {
	int totalBullets = 0;

	// Sumamos al total o sustituimos el total
	if(_relative == true) {
		totalBullets = _gameLogic->getBullets() + _amount;

		if(_amount > 0) {
			// If it's already playing just skip the animation
			if(!_bulletUp->IsPlaying()) _bulletUp->Begin();
		}
		else{
			// Only if there's no other animation here
			if(!_bulletDown->IsPlaying() && !_bulletDown->IsPlaying()) _bulletDown->Begin();
		}
	}
	else {
		totalBullets = _amount;
	}

	std::stringstream ss;
	ss << totalBullets;

	_gameLogic->setBullets(totalBullets);
	_bulletText->SetText(ss.str().c_str());

	return Action::ACTION_FINISHED;
}
