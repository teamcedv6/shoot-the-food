#include "ChangeRoundTime.h"

Noesis::TextBlock * ChangeRoundTime::_roundText = nullptr;
Noesis::Storyboard * ChangeRoundTime::_fewTimeAnim = nullptr;

ChangeRoundTime::ChangeRoundTime() : ChangeRoundTime(0) {}

ChangeRoundTime::ChangeRoundTime(float amount) : _amount(amount) {
	assert(_roundText != nullptr);
	assert(_fewTimeAnim != nullptr);
}

ChangeRoundTime::ChangeRoundTime(const ChangeRoundTime & changeLifes) {
	_amount = changeLifes._amount;
}

ChangeRoundTime::ChangeRoundTime(const ChangeRoundTime && changeLifes) {
	_amount = changeLifes._amount;
}

ChangeRoundTime::~ChangeRoundTime() {}

ChangeRoundTime & ChangeRoundTime::operator= (const ChangeRoundTime & changeLifes) {
	_amount = changeLifes._amount;

	return *this;
}

void ChangeRoundTime::setFewTimeAnim(Noesis::Storyboard * animation) { _fewTimeAnim = animation; }

void ChangeRoundTime::setRoundText(Noesis::TextBlock * text) { _roundText = text; }

Action::ActionResult ChangeRoundTime::update(Ogre::Real deltaTime) {
	if(_amount <= 3) {
		if(_fewTimeAnim->IsPlaying()) _fewTimeAnim->Stop();
		_fewTimeAnim->Begin();
	}

	std::stringstream ss;
	ss << _amount;

	_roundText->SetText(ss.str().c_str());

	return Action::ACTION_FINISHED;
}
