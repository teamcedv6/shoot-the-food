#include "AddImpulse.h"

AddImpulse::AddImpulse(){}

AddImpulse::AddImpulse(std::shared_ptr<Entity> object,PhysicSystem* phSystem, Ogre::Vector3 vector) :
		_phSystem(phSystem), _object(object), _vector(vector) {}

AddImpulse::AddImpulse(const AddImpulse & addImpulse) {
	_object = addImpulse._object;
	_phSystem = addImpulse._phSystem;
	_vector = addImpulse._vector;
}

AddImpulse::AddImpulse(const AddImpulse && addImpulse) {
	_object = addImpulse._object;
	_phSystem = addImpulse._phSystem;
	_vector = addImpulse._vector;
}

AddImpulse::~AddImpulse() {}

AddImpulse & AddImpulse::operator= (const AddImpulse & addImpulse) {
	_object = addImpulse._object;
	_phSystem = addImpulse._phSystem;
	_vector = addImpulse._vector;

	return *this;
}


Action::ActionResult AddImpulse::update(Ogre::Real deltaTime) {

	//if(_phSystem->checkCollisions(_me->getId(),_object->getId())){
		_phSystem->addImpulse(_object->getId(),_vector);
	//}

	_hasFinished = true;

	return Action::ACTION_FINISHED;
}
