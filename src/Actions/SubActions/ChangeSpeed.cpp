#include "ChangeSpeed.h"

Noesis::Storyboard * ChangeSpeed::_fasterAnim = nullptr;
Noesis::TextBlock * ChangeSpeed::_fasterText = nullptr;
std::shared_ptr<GameLogic> ChangeSpeed::_gameLogic = nullptr;

ChangeSpeed::ChangeSpeed() : ChangeSpeed(0) {

}

ChangeSpeed::ChangeSpeed(float amount) : _amount(amount) {
	assert(_gameLogic != nullptr);
	assert(_fasterAnim != nullptr);
}

ChangeSpeed::ChangeSpeed(const ChangeSpeed & changeLifes) {
	_amount = changeLifes._amount;
}

ChangeSpeed::ChangeSpeed(const ChangeSpeed && changeLifes) {
	_amount = changeLifes._amount;
}

ChangeSpeed::~ChangeSpeed() {}

ChangeSpeed & ChangeSpeed::operator= (const ChangeSpeed & changeLifes) {
	_amount = changeLifes._amount;

	return *this;
}

void ChangeSpeed::setGameLogic(std::shared_ptr<GameLogic> gameLogic) {_gameLogic = gameLogic;}

void ChangeSpeed::setFasterAnim(Noesis::Storyboard * animation) { _fasterAnim = animation; }

void ChangeSpeed::setFasterText(Noesis::TextBlock * text) { _fasterText = text; }

Action::ActionResult ChangeSpeed::update(Ogre::Real deltaTime) {

	_gameLogic->addDelayTime(_amount);


	return Action::ACTION_FINISHED;
}
