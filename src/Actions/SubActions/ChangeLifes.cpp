#include "ChangeLifes.h"

std::vector<Noesis::Storyboard *> ChangeLifes::_lifesUp;
std::vector<Noesis::Storyboard *> ChangeLifes::_lifesDown;
std::shared_ptr<GameLogic> ChangeLifes::_gameLogic = nullptr;

ChangeLifes::ChangeLifes() : ChangeLifes(0, true) {

}

ChangeLifes::ChangeLifes(int amount, bool relative) : _amount(amount), _relative(relative) {
	assert(_gameLogic != nullptr);
}

ChangeLifes::ChangeLifes(const ChangeLifes & changeLifes) {
	_amount = changeLifes._amount;
	_relative = changeLifes._relative;
}

ChangeLifes::ChangeLifes(const ChangeLifes && changeLifes) {
	_amount = changeLifes._amount;
	_relative = changeLifes._relative;
}

ChangeLifes::~ChangeLifes() {}

ChangeLifes & ChangeLifes::operator= (const ChangeLifes & changeLifes) {
	_amount = changeLifes._amount;
	_relative = changeLifes._relative;

	return *this;
}

void ChangeLifes::setGameLogic(std::shared_ptr<GameLogic> gameLogic) {_gameLogic = gameLogic;}

void ChangeLifes::addLifeUpAnimation(Noesis::Storyboard * lifeUp) {
	_lifesUp.push_back(lifeUp);
}

void ChangeLifes::addLifeDownAnimation(Noesis::Storyboard * lifeDown) {
	_lifesDown.push_back(lifeDown);
}

void ChangeLifes::clearAnimations() {
	_lifesUp.clear();
	_lifesDown.clear();
}


Action::ActionResult ChangeLifes::update(Ogre::Real deltaTime) {
	int totalLifes = 0;
	int previousLifes = _gameLogic->getLifes();

	// Sumamos al total o sustituimos el total
	if(_relative == true && (previousLifes + _amount) > 0 && (previousLifes + _amount) <= 5 ) {

		totalLifes = previousLifes + _amount;

		if(_amount > 0) {
			// Don't try to play two animations at the same time
			if(_lifesDown[totalLifes]->IsPlaying()) _lifesDown[totalLifes]->Stop();
			_lifesUp[totalLifes]->Begin();
		}
		else if (_amount < 0){
			// Don't try to play two animations at the same time
			if(_lifesUp[previousLifes-1]->IsPlaying()) _lifesUp[previousLifes-1]->Stop();
			_lifesDown[previousLifes-1]->Begin();
		}
	}
	else {
		if(_amount > 0 && _amount <= 5)
			totalLifes = _amount;
	}

	std::stringstream ss;
	ss << totalLifes;

	_gameLogic->setLifes(totalLifes);

	std::cout << totalLifes << "\n";
	if(totalLifes == 0) return Action::PLAYER_LOST;
	else return Action::ACTION_FINISHED;
}
