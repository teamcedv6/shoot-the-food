#include "ChangeElapsedTime.h"

Noesis::TextBlock * ChangeElapsedTime::_timeText = nullptr;

ChangeElapsedTime::ChangeElapsedTime() : ChangeElapsedTime(0) {

}

ChangeElapsedTime::ChangeElapsedTime(float amount) : _amount(amount) {
	assert(_timeText != nullptr);
}

ChangeElapsedTime::ChangeElapsedTime(const ChangeElapsedTime & changeLifes) {
	_amount = changeLifes._amount;
}

ChangeElapsedTime::ChangeElapsedTime(const ChangeElapsedTime && changeLifes) {
	_amount = changeLifes._amount;
}

ChangeElapsedTime::~ChangeElapsedTime() {}

ChangeElapsedTime & ChangeElapsedTime::operator= (const ChangeElapsedTime & changeLifes) {
	_amount = changeLifes._amount;

	return *this;
}

void ChangeElapsedTime::setTimeText(Noesis::TextBlock * text) { _timeText = text; }

Action::ActionResult ChangeElapsedTime::update(Ogre::Real deltaTime) {
	int s = 0;
	int min = 0;

	while(_amount > 60) { min++; _amount -= 60; }

	s = _amount;

	std::stringstream ss;
	if(min < 10) ss << '0';
	ss << min << ":";
	if(s < 10) ss << '0';
	ss << s;

	_timeText->SetText(ss.str().c_str());

	return Action::ACTION_FINISHED;
}
