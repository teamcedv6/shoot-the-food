#include "ChangeScore.h"

Noesis::TextBlock * ChangeScore::_scoreText = nullptr;
std::shared_ptr<GameLogic> ChangeScore::_gameLogic = nullptr;

ChangeScore::ChangeScore() : ChangeScore(0, true) {

}

ChangeScore::ChangeScore(int amount, bool relative) : _amount(amount), _relative(relative) {
	assert(_scoreText != nullptr);
	assert(_gameLogic != nullptr);
}

ChangeScore::ChangeScore(const ChangeScore & changeScore) {
	_amount = changeScore._amount;
	_relative = changeScore._relative;
}

ChangeScore::ChangeScore(const ChangeScore && changeScore) {
	_amount = changeScore._amount;
	_relative = changeScore._relative;
}

ChangeScore::~ChangeScore() {}

ChangeScore & ChangeScore::operator= (const ChangeScore & changeScore) {
	_amount = changeScore._amount;
	_relative = changeScore._relative;

	return *this;
}

void ChangeScore::setScoreText(Noesis::TextBlock * scoreText) {	_scoreText = scoreText;}

void ChangeScore::setGameLogic(std::shared_ptr<GameLogic> gameLogic) {_gameLogic = gameLogic;}

Action::ActionResult ChangeScore::update(Ogre::Real deltaTime) {
	int totalScore = 0;

	// Sumamos al total o sustituimos el total
	if(_relative == true) {
		totalScore = _gameLogic->getScore() + _amount;
	}
	else {
		totalScore = _amount;
	}

	std::stringstream ss;
	ss << totalScore;

	_gameLogic->setScore(totalScore);
	_scoreText->SetText(ss.str().c_str());

	return Action::ACTION_FINISHED;
}
