/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/


#include "GameManager.h"
#include "GameState.h"

#include "MainMenuState.h"
#include "IngameMenuState.h"
#include "AuthorState.h"
#include "ScoreState.h"
#include "OptionsState.h"
#include "LoadState.h"
#include "PlayState.h"
#include "PauseState.h"

#include <iostream>

using namespace std;

 int main () {

	// Game Manager - only instance
	auto gameManagerPtr = make_shared<GameManager>();

	// Game States - only instance
	auto mainMenuStatePtr = std::make_shared<MainMenuState>(gameManagerPtr);
	auto ingameMenuStatePtr = std::make_shared<IngameMenuState>(gameManagerPtr);
	auto authorStatePtr = std::make_shared<AuthorState>(gameManagerPtr);
	auto scoreStatePtr = std::make_shared<ScoreState>(gameManagerPtr);
	auto optionsStatePtr = std::make_shared<OptionsState>(gameManagerPtr);
	auto loadStatePtr = std::make_shared<LoadState>(gameManagerPtr);
	auto playStatePtr = std::make_shared<PlayState>(gameManagerPtr);
	auto pauseStatePtr = std::make_shared<PauseState>(gameManagerPtr);

	// Map Game States
	gameManagerPtr->addNewState(GameState::States::MAIN_MENU,mainMenuStatePtr);
	gameManagerPtr->addNewState(GameState::States::INGAME_MENU,ingameMenuStatePtr);
	gameManagerPtr->addNewState(GameState::States::AUTHOR,authorStatePtr);
	gameManagerPtr->addNewState(GameState::States::SCORE,scoreStatePtr);
	gameManagerPtr->addNewState(GameState::States::OPTIONS,optionsStatePtr);
	gameManagerPtr->addNewState(GameState::States::LOAD,loadStatePtr);
	gameManagerPtr->addNewState(GameState::States::PLAY,playStatePtr);
	gameManagerPtr->addNewState(GameState::States::PAUSE,pauseStatePtr);



	// Main menu
	function<void(InputBindings::PushState)> fMainExit = std::bind(&MainMenuState::exitGame, mainMenuStatePtr.get(), std::placeholders::_1);
	mainMenuStatePtr->getInputBindings()->setHook(OIS::KC_ESCAPE, fMainExit);

	// Play
	function<void(InputBindings::PushState)> fPlayExit = std::bind(&PlayState::exitGame, playStatePtr.get(), std::placeholders::_1);
	function<void(const OIS::MouseEvent &)>  fMovePov = std::bind(&PlayState::movePoV, playStatePtr.get(), std::placeholders::_1);
	function<void(const OIS::MouseEvent &, InputBindings::PushState)> fShoot =std::bind(&PlayState::shoot, playStatePtr.get(), std::placeholders::_1, std::placeholders::_2);
	playStatePtr->getInputBindings()->setHook(OIS::KC_ESCAPE, fPlayExit);
	playStatePtr->getInputBindings()->setHook(fMovePov);
	playStatePtr->getInputBindings()->setHook(OIS::MouseButtonID::MB_Left, fShoot);

	try {
		// Init Game
		gameManagerPtr->start(mainMenuStatePtr);
	} catch (Ogre::Exception& e) {
		std::cerr << "Exception detected: " << e.getFullDescription();
	}

	// TODO: Ver liberación de memoria en GameManager e InputManager (No pasa por los destructores, hacer release?)

	return 0;
}
