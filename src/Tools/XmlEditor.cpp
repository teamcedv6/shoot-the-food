#include "XmlEditor.h"

XmlEditor::XmlEditor(): _xmlDoc(nullptr), _load(true), _write(true) {}

XmlEditor::XmlEditor(const XmlEditor & xmlEditor) {
	_xmlDoc = xmlEditor._xmlDoc;
	_load = xmlEditor._load;
	_write = xmlEditor._write;
}

XmlEditor::XmlEditor(const XmlEditor && xmlEditor) {
	_xmlDoc = xmlEditor._xmlDoc;
	_load = xmlEditor._load;
	_write = xmlEditor._write;
}

XmlEditor::~XmlEditor () {}

XmlEditor & XmlEditor::operator= (const XmlEditor & xmlEditor) {
	_xmlDoc = xmlEditor._xmlDoc;
	_load = xmlEditor._load;
	_write = xmlEditor._write;

	return *this;
}

void XmlEditor::loadDocument(const Ogre::String &documentName, const Ogre::String &groupName, const Ogre::String &rootDocument) {
	try {
		// Strip the path
		Ogre::String basename, path;
		Ogre::StringUtil::splitFilename(documentName, basename, path);

		Ogre::DataStreamPtr pStream = Ogre::ResourceGroupManager::getSingleton().
		openResource( basename, groupName );

		Ogre::String data = pStream->getAsString();
		// Open the .xml File
		std::shared_ptr<TiXmlDocument> smartXmlDoc(new TiXmlDocument());
		_xmlDoc = smartXmlDoc;
		_xmlDoc->Parse( data.c_str() );
		pStream->close();
		pStream.setNull();

		if( _xmlDoc->Error() ) {
			//We'll just log, and continue on gracefully
			Ogre::LogManager::getSingleton().logMessage("[XmlEditor] The TiXmlDocument reported an error");
			_load = false;
		}
	} catch(...) {
		//We'll just log, and continue on gracefully
		Ogre::LogManager::getSingleton().logMessage("[XmlEditor] Error creating TiXmlDocument");
		_load = false;
	}

	// Validate the File

	if( _xmlDoc->RootElement()->Value() != rootDocument  ) {
		Ogre::LogManager::getSingleton().logMessage( "[XmlEditor] Error: Invalid .xml File. Missing <" + rootDocument + ">" );
		_load = false;
	}
}

std::shared_ptr<TiXmlDocument> XmlEditor::getXmlDoc() {
	return _xmlDoc;
}

void XmlEditor::writeDocument(std::shared_ptr<TiXmlDocument> document,const Ogre::String &dir, const Ogre::String &documentName, bool back) {
	Ogre::String file = dir + documentName;
	if(back)
		file += ".back";
	document->SaveFile(file.c_str());
}

bool XmlEditor::isLoad() {
	return _load;
}

bool XmlEditor::isWrite() {
	return _write;
}


