#include "ActionsManager.h"

#include "PlayState.h"

ActionsManager::ActionsManager(const ActionsManager & actionsManager) {}

ActionsManager::ActionsManager(const ActionsManager && actionsManager) {}

ActionsManager::~ActionsManager () {}

ActionsManager & ActionsManager::operator= (const ActionsManager & actionsManager) { return *this; }

void ActionsManager::addAction(shared_ptr<Action> action) { _actions.push_back(action);}

void ActionsManager::update(Ogre::Real deltaTime) {
	vector<int> finished;
	Action::ActionResult result;

	finished.reserve(_actions.size());
	int i = 0;

	for(auto it = _actions.begin(); it != _actions.end(); it++, i++) {
		result = (*it)->update(deltaTime);
		//result = Action::CONTINUE;

		switch(result){
			case Action::CONTINUE:
			break;
			case Action::PLAYER_WIN:
			break;
			case Action::PLAYER_LOST:
				finished.push_back(i);
				static_cast<PlayState*>(_playState.get())->setLose(true);
				std::cout << "*********************************** GAME OVER *********************************************\n";
			break;
			case Action::ACTION_FINISHED:
				finished.push_back(i);
			break;
		}

	}

	for(int i : finished) {
		_actions.erase(_actions.begin() + i);
	}
}

