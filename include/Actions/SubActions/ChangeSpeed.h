/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CHANGE_SPEED_H
#define CHANGE_SPEED_H

#include "Ogre.h"

#include "Subaction.h"
#include "NoesisGUI.h"
#include "GameLogic.h"

class ChangeSpeed : public Subaction {
public:

	ChangeSpeed();
	ChangeSpeed(float amount);
	ChangeSpeed(const ChangeSpeed & changeSpeed);
	ChangeSpeed(const ChangeSpeed && changeSpeed);
	virtual ~ChangeSpeed();

	ChangeSpeed & operator= (const ChangeSpeed & changeSpeed);

	static void setGameLogic(std::shared_ptr<GameLogic> gameLogic);
	static void setFasterAnim(Noesis::Storyboard * animation);
	static void setFasterText(Noesis::TextBlock * text);

	Action::ActionResult update(Ogre::Real deltaTime) override;

private:
	float _amount;

	static Noesis::TextBlock * _fasterText;
	static Noesis::Storyboard * _fasterAnim;
	static std::shared_ptr<GameLogic> _gameLogic;
};

#endif /* CHANGE_SPEED_H */
