/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CHANGE_LIFES_H
#define CHANGE_LIFES_H

#include "Ogre.h"

#include "Subaction.h"
#include "NoesisGUI.h"
#include "GameLogic.h"

class ChangeLifes : public Subaction {
public:

	ChangeLifes();
	ChangeLifes(int amount, bool relative);
	ChangeLifes(const ChangeLifes & changeLifes);
	ChangeLifes(const ChangeLifes && changeLifes);
	virtual ~ChangeLifes();

	ChangeLifes & operator= (const ChangeLifes & changeLifes);


	static void setGameLogic(std::shared_ptr<GameLogic> gameLogic);
	static void addLifeUpAnimation(Noesis::Storyboard * lifeUp);
	static void addLifeDownAnimation(Noesis::Storyboard * lifeDown);
	static void clearAnimations();

	Action::ActionResult update(Ogre::Real deltaTime) override;

private:
	bool _relative;
	int _amount;

	static std::vector<Noesis::Storyboard *> _lifesUp;
	static std::vector<Noesis::Storyboard *> _lifesDown;
	static std::shared_ptr<GameLogic> _gameLogic;
};

#endif /* CHANGE_LIFES_H */
