/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CHANGE_ROUND_TIME_H
#define CHANGE_ROUND_TIME_H

#include "Ogre.h"

#include "Subaction.h"
#include "NoesisGUI.h"
#include "GameLogic.h"

class ChangeRoundTime : public Subaction {
public:

	ChangeRoundTime();
	ChangeRoundTime(float amount);
	ChangeRoundTime(const ChangeRoundTime & changeRoundTime);
	ChangeRoundTime(const ChangeRoundTime && changeRoundTime);
	virtual ~ChangeRoundTime();

	ChangeRoundTime & operator= (const ChangeRoundTime & changeRoundTime);

	static void setFewTimeAnim(Noesis::Storyboard * animation);
	static void setRoundText(Noesis::TextBlock * text);

	Action::ActionResult update(Ogre::Real deltaTime) override;

private:
	float _amount;

	static Noesis::TextBlock * _roundText;
	static Noesis::Storyboard * _fewTimeAnim;
};

#endif /* CHANGE_ROUND_TIME_H */
