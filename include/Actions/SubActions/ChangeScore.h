/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CHANGE_SCORE_H
#define CHANGE_SCORE_H

#include "Ogre.h"

#include "Subaction.h"
#include "NoesisGUI.h"
#include "GameLogic.h"

class ChangeScore : public Subaction {
public:

	ChangeScore();
	ChangeScore(int amount, bool relative);
	ChangeScore(const ChangeScore & changeScore);
	ChangeScore(const ChangeScore && changeScore);
	virtual ~ChangeScore();

	ChangeScore & operator= (const ChangeScore & changeScore);

	static void setScoreText(Noesis::TextBlock * scoreText);
	static void setGameLogic(std::shared_ptr<GameLogic> gameLogic);

	Action::ActionResult update(Ogre::Real deltaTime) override;

private:
	bool _relative;
	int _amount;

	static Noesis::TextBlock * _scoreText;
	static std::shared_ptr<GameLogic> _gameLogic;
};

#endif /* CHANGE_SCORE_H */
