/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CHANGE_ELAPSED_TIME_H
#define CHANGE_ELAPSED_TIME_H

#include "Ogre.h"

#include "Subaction.h"
#include "NoesisGUI.h"
#include "GameLogic.h"

class ChangeElapsedTime : public Subaction {
public:

	ChangeElapsedTime();
	ChangeElapsedTime(float amount);
	ChangeElapsedTime(const ChangeElapsedTime & changeElapsedTime);
	ChangeElapsedTime(const ChangeElapsedTime && changeElapsedTime);
	virtual ~ChangeElapsedTime();

	ChangeElapsedTime & operator= (const ChangeElapsedTime & changeElapsedTime);

	static void setTimeText(Noesis::TextBlock * text);
	Action::ActionResult update(Ogre::Real deltaTime) override;

private:
	float _amount;
	static Noesis::TextBlock * _timeText;
};

#endif /* CHANGE_ELAPSED_TIME_H */
