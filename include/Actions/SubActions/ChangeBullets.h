/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CHANGE_BULLETS_H
#define CHANGE_BULLETS_H

#include "Ogre.h"

#include "Subaction.h"
#include "NoesisGUI.h"
#include "GameLogic.h"

class ChangeBullets : public Subaction {
public:

	ChangeBullets();
	ChangeBullets(int amount, bool relative);
	ChangeBullets(const ChangeBullets & changeBullets);
	ChangeBullets(const ChangeBullets && changeBullets);
	virtual ~ChangeBullets();

	ChangeBullets & operator= (const ChangeBullets & changeBullets);

	static void setBulletText(Noesis::TextBlock * scoreText);
	static void setBulletUp(Noesis::Storyboard * bulletUp);
	static void setBulletDown(Noesis::Storyboard * bulletDown);
	static void setGameLogic(std::shared_ptr<GameLogic> gameLogic);

	Action::ActionResult update(Ogre::Real deltaTime) override;



private:
	bool _relative;
	int _amount;

	static Noesis::TextBlock * _bulletText;
	static Noesis::Storyboard * _bulletDown;
	static Noesis::Storyboard * _bulletUp;
	static std::shared_ptr<GameLogic> _gameLogic;
};

#endif /* CHANGE_BULLETS_H */
