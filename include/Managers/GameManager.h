/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include <stack>
#include <map>
#include <string>

#include <Ogre.h>
#include <OgreSingleton.h>
#include <OIS.h>

#include "Configuration.h"

#include "InputManager.h"

#include "NodeManager.h"
#include "ActionsManager.h"
#include "GraphicSystem.h"
#include "CameraSystem.h"
#include "PhysicSystem.h"
#include "AnimationSystem.h"
#include "MovementSystem.h"
#include "UserInterfaceSystem.h"

#include "SoundManager.h"

using namespace std;

class GameState;

class GameManager : public Ogre::FrameListener, public OIS::KeyListener, public OIS::MouseListener {

	public:

		GameManager():_root(0){}
		~GameManager ();

		// Init
		void start (shared_ptr<GameState> state);

		// States changes
		void changeState (shared_ptr<GameState> state);
		void pushState (shared_ptr<GameState> state);
		void popState ();

		// Get/Set Unique States
		void addNewState(int id, shared_ptr<GameState> state);
		shared_ptr<GameState> getState(int id);

		// Getters
		shared_ptr<Ogre::Root> getRoot();
		shared_ptr<Ogre::SceneManager> getSceneManager();
		shared_ptr<Ogre::RenderWindow> getRenderWindow();
		shared_ptr<Ogre::Viewport> getViewport();
		shared_ptr<Ogre::Camera> getCamera();

		shared_ptr<NodeManager> getNodeManager();
		shared_ptr<ActionsManager> getActionsManager();
		shared_ptr<SoundManager> getSoundManager();

		shared_ptr<Configuration> getConfig();

		shared_ptr<GraphicSystem> getGraphicsSystem();
		shared_ptr<PhysicSystem> getPhysicsSystem();
		shared_ptr<AnimationSystem> getAnimationSystem();
		shared_ptr<CameraSystem> getCameraSystem();
		shared_ptr<MovementSystem> getMovementSystem();
		std::shared_ptr<IdNameMapper> getMapper();
		shared_ptr<UserInterfaceSystem> getUserInterfaceSystem();
		shared_ptr<GameLogic> getGameLogic();

		void setEntity(const std::string & name, std::shared_ptr<Entity> entity){(*_entities)[name] = entity;}
		std::shared_ptr<Entity> getEntity(const std::string & name) { return (*_entities)[name]; }
		void deleteEntity(const std::string & name) {}

		shared_ptr<InputManager> getInputManager(){return _inputManager;}

		// Setters
		void setCamera(shared_ptr<Ogre::Camera> camera);
		void setViewPort(shared_ptr<Ogre::Viewport> viewport);
		shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> _entities;
		void setGameLogic(shared_ptr<GameLogic> gameLogic);


	protected:

		// Ogre Objects
		shared_ptr<Ogre::Root> _root;
		shared_ptr<Ogre::SceneManager> _sceneManager;
		shared_ptr<Ogre::RenderWindow> _renderWindow;
		shared_ptr<Ogre::Viewport> _viewport;
		shared_ptr<Ogre::Camera> _camera;

		// Ogre Config
		void loadResources ();
		bool configure ();

		// Game Loop
		bool frameStarted (const Ogre::FrameEvent& evt) override;
		bool frameEnded (const Ogre::FrameEvent& evt) override;
		bool frameRenderingQueued(const Ogre::FrameEvent & evt) override;

	private:

		// OIS listeners
		bool keyPressed (const OIS::KeyEvent &e);
		bool keyReleased (const OIS::KeyEvent &e);
		bool mouseMoved (const OIS::MouseEvent &e);
		bool mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

		// Input Manager
		shared_ptr<InputManager> _inputManager;

		// Game States - Stack States and Map Unique Instace States
		std::stack<shared_ptr<GameState>> _states;
		std::map<int,shared_ptr<GameState>> _existingStates;

		// Config
		std::shared_ptr<Configuration> _config;

		// GameLobic
		std::shared_ptr<GameLogic> _gameLogic;

		// Id mapper
		std::shared_ptr<IdNameMapper> _idMapper;

		// Systems that the game need
		std::shared_ptr<NodeManager> _nodes;
		std::shared_ptr<ActionsManager> _actions;
		std::shared_ptr<SoundManager> _sound;

		std::shared_ptr<GraphicSystem> _graphics;
		std::shared_ptr<PhysicSystem> _physics;
		std::shared_ptr<AnimationSystem> _animations;
		std::shared_ptr<CameraSystem> _cameras;
		std::shared_ptr<MovementSystem> _movement;
		std::shared_ptr<UserInterfaceSystem> _ui;

};

#endif /* END GAME_MANAGER_H */
