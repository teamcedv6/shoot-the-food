/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef ACTIONS_MANAGER_H_
#define ACTIONS_MANAGER_H_

#include <vector>
#include "Ogre.h"

#include "Action.h"

using namespace std;

class GameState;


class ActionsManager {
public:

	ActionsManager(std::shared_ptr<GameState> playState ): _playState(playState){}

	ActionsManager(const ActionsManager & actionsManager);
	ActionsManager(const ActionsManager && actionsManager);
	~ActionsManager ();

	ActionsManager & operator= (const ActionsManager & actionsManager);

	void addAction(shared_ptr<Action> action);
	void update(Ogre::Real deltaTime);

private:
	vector<shared_ptr<Action>> _actions;
	std::shared_ptr<GameState> _playState;

};
#endif /* END ACTIONS_MANAGER_H_ */
