/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H

#include <Ogre.h>
#include <OIS.h>

#include "Configuration.h"

using namespace std;

class InputManager : public OIS::KeyListener, public OIS::MouseListener {
	public:

		InputManager(std::shared_ptr<Configuration> configuration):_configuration(configuration), _inputSystem(0), _keyboard(0), _mouse(0){}
		virtual ~InputManager ();

		void initialise (shared_ptr<Ogre::RenderWindow> renderWindow);
		void capture ();

		// Listeners manage
		void addKeyListener (shared_ptr<OIS::KeyListener> keyListener, const std::string& instanceName);
		void addMouseListener (shared_ptr<OIS::MouseListener> mouseListener, const std::string& instanceName );
		void removeKeyListener (const std::string& instanceName);
		void removeMouseListener (const std::string& instanceName);
		void removeKeyListener (shared_ptr<OIS::KeyListener> keyListener);
		void removeMouseListener (shared_ptr<OIS::MouseListener> mouseListener);

		void removeAllListeners ();
		void removeAllKeyListeners ();
		void removeAllMouseListeners ();

		void setWindowExtents (int width, int height);

		shared_ptr<OIS::Keyboard> getKeyboard ();
		shared_ptr<OIS::Mouse> getMouse ();

	private:

		bool keyPressed (const OIS::KeyEvent &e);
		bool keyReleased (const OIS::KeyEvent &e);
		bool mouseMoved (const OIS::MouseEvent &e);
		bool mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
		bool mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

		std::shared_ptr<Configuration> _configuration;

		OIS::InputManager* _inputSystem;
		shared_ptr<OIS::Keyboard> _keyboard;
		shared_ptr<OIS::Mouse> _mouse;

		std::map<std::string, shared_ptr<OIS::KeyListener>> _keyListeners;
		std::map<std::string, shared_ptr<OIS::MouseListener>> _mouseListeners;
		std::map<std::string, shared_ptr<OIS::KeyListener>>::iterator itKeyListener;
		std::map<std::string, shared_ptr<OIS::MouseListener>>::iterator itMouseListener;
		std::map<std::string, shared_ptr<OIS::KeyListener>>::iterator itKeyListenerEnd;
		std::map<std::string, shared_ptr<OIS::MouseListener>>::iterator itMouseListenerEnd;
};
#endif /* END INPUT_MANAGER_H */
