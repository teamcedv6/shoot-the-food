/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef GAME_LOGIC_H
#define GAME_LOGIC_H

#include <map>
#include <vector>
#include <random>
#include <string>
#include <tuple>

#include "Ogre.h"

#include "SoundManager.h"

#include "Obj.h"

#include "pch.h"

#include "ChangeRoundTime.h"
#include "ChangeElapsedTime.h"

class PhysicSystem;

// Game Logic
class GameLogic{

	public:
		typedef struct Options {
			Ogre::Vector3 launcher;
			Ogre::Vector3 vector;
		}Options_t;

		GameLogic();
		GameLogic(std::shared_ptr<Configuration> configuration,std::shared_ptr<SoundManager> soundManager,
				std::shared_ptr<Ogre::SceneManager> sceneManager, std::shared_ptr<NodeManager> nodeManager);
		GameLogic(const GameLogic & gameLogic);
		GameLogic(const GameLogic && gameLogic);
		~GameLogic();
		GameLogic& operator = (const GameLogic &gameLogic);

		void resetGame();

		int getLifes();
		int getScore();
		int getBullets();
		int getRounds();
		Ogre::Real getTotalTiem();

		void setScore(int score);
		void setLifes(int lifes);
		void setBullets(int bullets);
		void setFasterAnim(Noesis::Storyboard * fasterAnim);

		void addDelayTime(float time);

		void update(std::shared_ptr<Configuration> config, std::shared_ptr<NodeManager> nodes,
				std::shared_ptr<GraphicSystem> graphic, PhysicSystem* physic,
				std::shared_ptr<ActionsManager> actions, std::shared_ptr<std::map<const std::string, std::shared_ptr<Entity>>> entities,
				Ogre::Real deltaTime);

	private:

		std::shared_ptr<Configuration> _configuration;
		std::shared_ptr<SoundManager> _soundManager;

		int _score;
		int _lifes;
		int _bulletNumber;

		std::map<std::string,Ogre::Real> _times;
		std::map<std::string,Ogre::Real> _splitTimes;
		std::map<std::string,Ogre::Real> _speedUps;
		std::map<std::string,bool> _activeCannons;
		std::map<std::string,Ogre::ParticleSystem*> _particleSystem;

		Ogre::Real _countDown;
		Ogre::Real _countDownInit;
		Ogre::Real _totalTime;

		Ogre::Real _elapsedCountDown;
		Ogre::Real _elapsedTotalTime;
		int _round;

		Noesis::Storyboard * _fasterAnim;

		int _getIntRandom(int init, int end);
		float _getRealRandom(float init, float end);
};

#endif /* GAME_LOGIC_H */
