/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <memory>
#include <iostream>

#include "XmlEditor.h"

// Options for Game
class Configuration{

	public:

		Configuration();
		Configuration(const Configuration & configuration);
		Configuration(const Configuration && configuration);
		~Configuration();
		Configuration& operator = (const Configuration &configuration);

		void loadConfig();
		void writeConfig();

		// Config Options
		typedef struct Physic {
			bool debug;
			float gravity;
			float worldBounds;
			float bodyRestitution;
			float bodyFriction;
			float bodyMass;
		} Physic_t;

		// Items Options
		typedef struct Item {
			std::string name;
			float bodyRestitution;
			float bodyFriction;
			float bodyMass;
			int pts;
			float value;
		} Item_t;

		// Items Options
		typedef struct Canon {
			bool active;
			Ogre::Real time;
			Ogre::Real splitTime;
			Ogre::Real speedUp;
			Ogre::Vector3 launchPosition;
			Ogre::Vector3 directonLauch;
		} Canon_t;

		// Setters Options
		void setPhysicConfig(bool debug, float gravity, float worldBounds, float bodyRestitution, float bodyFriction, float bodyMass);
		void setMusic(std::string key, std::string value);
		void setFx(std::string key, std::string value);
		void setFood(int key, std::shared_ptr<Item_t> value);
		void setPowerUp(int key, std::shared_ptr<Item_t> value);
		void setCustom(int key, std::shared_ptr<Item_t> value);
		void setCanon(std::string name, Canon_t value);
		void setCountDown(Ogre::Real countDown);
		void setMouseGrab(bool active);


		// Getters
		std::shared_ptr<Physic_t> getPhysicConfig();
		std::shared_ptr<std::map<std::string, std::string>> getMusicList();
		std::shared_ptr<std::map<std::string, std::string>> getFxList();
		std::shared_ptr<std::map<int, std::shared_ptr<Item_t>>> getFoodList();
		std::shared_ptr<std::map<int, std::shared_ptr<Item_t>>> getPowerUpList();
		std::shared_ptr<std::map<int, std::shared_ptr<Item_t>>> getCustomList();
		std::map<std::string,Canon_t> getCanonList();
		Ogre::Real getCountDown();
		std::string getMouseGrab();


		int getKeyRandomName();


	private:

		bool _proccessConfig(TiXmlElement* rootXml);
		void _proccessPhysic(TiXmlElement* physic);
		void _proccessSound(TiXmlElement* sound);
		void _proccessFood(TiXmlElement* food);
		void _proccessPowerUp(TiXmlElement* powerup);
		void _proccessCustom(TiXmlElement* custom);
		void _proccessGameLogic(TiXmlElement* gameLogic);
		void _getAttributes(std::shared_ptr<Item_t> item, TiXmlElement* element);

		std::shared_ptr<Physic_t> _physicConfig;
		std::shared_ptr<std::map<std::string, std::string>> _musicList;
		std::shared_ptr<std::map<std::string, std::string>> _fxList;
		std::shared_ptr<std::map<int, std::shared_ptr<Item_t>>> _foodList;
		std::shared_ptr<std::map<int, std::shared_ptr<Item_t>>> _powerUpList;
		std::shared_ptr<std::map<int, std::shared_ptr<Item_t>>> _customList;
		std::map<std::string,Canon_t> _cannonList;

		Ogre::Real _countDown;
		std::string _mouseGrab;
		int _keyRandomName;


};
#endif /* CONFIGURATION_H */
