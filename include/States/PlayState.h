/*********************************************************************
 * Shoot the Food - CEDV
 *
 * Autores:
 * Gonzalo Trigueros Manzanas			gtriguerosmanzanas@gmail.com
 * Jose Antonio Costa de Moya			joseantonio.costa250@gmail.com
 * David Delgado Lizcano				daviddelgadolizcano@gmail.com
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef PLAY_STATE_H
#define PLAY_STATE_H

#include <Ogre.h>
#include <OIS.h>

#include "GameState.h"
#include "GameManager.h"

#include "MovePoV.h"
#include "Obj.h"
#include "AddImpulse.h"
#include "GameLogic.h"
#include <memory>

#include "ChangeBullets.h"
#include "ChangeLifes.h"
#include "ChangeScore.h"
#include "ChangeSpeed.h"
#include "ChangeRoundTime.h"
#include "ChangeElapsedTime.h"

class PlayState : public GameState {

	public:

		PlayState(shared_ptr<GameManager> gameManager): GameState(gameManager), _fov(nullptr),
		_nodes(nullptr),_actions(nullptr),_grSystem(nullptr),_phSystem(nullptr),_aSystem(nullptr),_cmSystem(nullptr),_move(nullptr),_lose(false), _exitGame(false) {}

		// States Actions
		void enter ();
		void exit ();
		void pause ();
		void resume ();

		void movePoV(const OIS::MouseEvent & e);
		void shoot(const OIS::MouseEvent & e, InputBindings::PushState state);
		void setLose(bool lose);

		// Game Loop
		bool frameStarted (const Ogre::FrameEvent& evt);
		bool frameEnded (const Ogre::FrameEvent& evt);

		void exitGame(InputBindings::PushState pushState);

	protected:
	
		std::shared_ptr<Entity> _fov;

		std::shared_ptr<NodeManager> _nodes;
		std::shared_ptr<ActionsManager> _actions;
		std::shared_ptr<GraphicSystem> _grSystem;
		std::shared_ptr<PhysicSystem> _phSystem;
		std::shared_ptr<AnimationSystem> _aSystem;
		std::shared_ptr<CameraSystem> _cmSystem;
		std::shared_ptr<MovementSystem> _move;
		std::shared_ptr<UserInterfaceSystem> _ui;
		std::shared_ptr<GameLogic> _gameLogic;
		bool _lose;
		bool _exitGame;

};

#endif /* END PLAY_STATE_H*/
